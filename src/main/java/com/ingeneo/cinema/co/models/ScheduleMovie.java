package com.ingeneo.cinema.co.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="schedulemovie")
public class ScheduleMovie {

	/*id único de la tabla usado para clave primaria*/
	@Id
	@NotNull
	@Column(name="id_schedulemovie")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@JoinColumn(name = "branchOffice_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private BranchOffice branchOffice;
	
	@JoinColumn(name = "movie_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Movie movie;
	
	@JoinColumn(name = "cinema_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Cinema cinema;
	
	@NotNull
	@Column(name="fecha",columnDefinition="DATE NOT NULL")
	private Date fecha;
	
	@NotNull
	@Column(name="hora",columnDefinition="VARCHAR(8) NOT NULL")
	private String hora;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BranchOffice getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(BranchOffice branchOffice) {
		this.branchOffice = branchOffice;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Cinema getCinema() {
		return cinema;
	}

	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	@Override
	public String toString() {
		return "ScheduleMovie [id=" + id + ", branchOffice=" + branchOffice + ", movie=" + movie + ", cinema=" + cinema
				+ ", fecha=" + fecha + ", hora=" + hora + "]";
	}
	

	
}
