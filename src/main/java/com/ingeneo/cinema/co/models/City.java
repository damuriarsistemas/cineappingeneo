package com.ingeneo.cinema.co.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="city")
public class City {
	
	/*id único de la tabla usado para clave primaria*/
	@Id
	@NotNull
	@Column(name="id_city")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	/*nombre de la ciudad*/
	@NotNull
	@Column(name="namecity", nullable=false , columnDefinition="varchar(35) NOT NULL" , length=35)
	private String nameCity;
	
	
	@JsonManagedReference
	@OneToMany(mappedBy="city", fetch = FetchType.EAGER)
	private List<BranchOffice> lstBranchOffice;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameCity() {
		return nameCity;
	}

	public void setNameCity(String nameCity) {
		this.nameCity = nameCity;
	}
	
	
	public List<BranchOffice> getLstBranchOffice() {
		return lstBranchOffice;
	}

	public void setLstBranchOffice(List<BranchOffice> lstBranchOffice) {
		this.lstBranchOffice = lstBranchOffice;
	}
	
	public void addCity_BranchOffice(BranchOffice obj){
        if(this.lstBranchOffice == null){
            this.lstBranchOffice = new ArrayList<>();
        }       
        this.lstBranchOffice.add(obj);
	}


	
	

}
