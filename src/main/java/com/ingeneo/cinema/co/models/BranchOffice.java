package com.ingeneo.cinema.co.models;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="branchoffice")
public class BranchOffice {

	/*id único de la tabla usado para clave primaria*/
	@Id
	@NotNull
	@Column(name="id_branchoffice")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	/*nombre de la sucursal*/
	@NotNull
	@Basic(optional=false)
	@Column(name="namebranchoffice", nullable=false , columnDefinition="varchar(30) NOT NULL", length=30)
	private String nameBranchOffice;

	/*dirección de localidad de la sucursal*/
	@NotNull
	@Basic(optional=false)
	@Column(name="address", nullable=false , columnDefinition="varchar(60) NOT NULL", length=60)
	private String address;
	
	
	/*administrador responsable de la sucursal*/
	@NotNull
	@JoinColumn(name = "user_id_manager", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private User userManager;
	
	/*ciudad a la cual esta asociada la sucursal*/
	@NotNull
	@JsonIgnore
	@JoinColumn(name = "city_id", nullable = false, referencedColumnName="id_city")
	@ManyToOne(fetch = FetchType.EAGER)
    private City city;

	@JsonIgnore
	@OneToMany(mappedBy="branchOffice", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Cinema> lstCinema;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameBranchOffice() {
		return nameBranchOffice;
	}

	public void setNameBranchOffice(String nameBranchOffice) {
		this.nameBranchOffice = nameBranchOffice;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public User getUserManager() {
		return userManager;
	}

	public void setUserManager(User userManager) {
		this.userManager = userManager;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	public List<Cinema> getLstCinema() {
		return lstCinema;
	}

	public void setLstCinema(List<Cinema> lstCinema) {
		this.lstCinema = lstCinema;
	}


}
