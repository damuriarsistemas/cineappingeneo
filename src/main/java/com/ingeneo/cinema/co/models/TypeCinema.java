package com.ingeneo.cinema.co.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="typecinema")
public class TypeCinema {
	
	/*id único de la tabla usado para clave primaria*/
	@Id
	@NotNull
	@Column(name="id_type_cinema")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	/*nombre de la ciudad*/
	@NotNull
	@Size(min=1, max=12)
	@Column(name="typeCinema", nullable=false , columnDefinition="varchar(12) NOT NULL" , length=12)
	private String typeCinema;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTypeCinema() {
		return typeCinema;
	}

	public void setTypeCinema(String typeCinema) {
		this.typeCinema = typeCinema;
	}

	@Override
	public String toString() {
		return "TypeCinema [id=" + id + ", typeCinema=" + typeCinema + "]";
	}

	
		
	

}
