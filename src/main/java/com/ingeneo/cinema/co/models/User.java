package com.ingeneo.cinema.co.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.ingeneo.cinema.co.global.Constantes;
import com.sun.istack.NotNull;

@Entity
@Table(schema="public",name="user")
public class User {
	
	/*id único de la tabla usado para clave primaria*/
	@Id
	@NotNull
	@Column(name="id_user")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	@Size(min=4, max=12)
	@Column(name="acount", nullable=false , columnDefinition="varchar(12) NOT NULL" , length=12)
	private String acount;


	@NotNull
	@Size(min=4, max=12)
	@Column(name="password", nullable=false , columnDefinition="varchar(12) NOT NULL" , length=12)
	private String password;
	
	@NotNull
	@Column(name="rolcode", nullable=false , columnDefinition="NUMERIC NOT NULL")
	private Integer rolcode;

	
	public User() {
		this.rolcode = Constantes.BASIC_USER;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAcount() {
		return acount;
	}

	public void setAcount(String acount) {
		this.acount = acount;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRolcode() {
		return rolcode;
	}

	public void setRolcode(Integer rolcode) {
		this.rolcode = rolcode;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", acount=" + acount + ", password=" + password + ", rolcode=" + rolcode + "]";
	}
	
	

}
