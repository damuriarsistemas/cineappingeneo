package com.ingeneo.cinema.co.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="additionalfield")
public class AdditionalField {
	
	/*id único de la tabla usado para clave primaria*/
	@Id
	@NotNull
	@Column(name="id_additionalfield")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	/*nombre de la ciudad*/
	@Column(name="additionalfield", nullable=false, columnDefinition="varchar(14) NOT NULL" , length=14)
	private String additionalField;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAdditionalField() {
		return additionalField;
	}

	public void setAdditionalField(String additionalField) {
		this.additionalField = additionalField;
	}

	@Override
	public String toString() {
		return "AdditionalField [id=" + id + ", additionalField=" + additionalField + "]";
	}

}
