package com.ingeneo.cinema.co.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="movie_branchoffice")
public class Movie_BranchOffice {
	/*
	 * contiene la relación de películas con sucursales, ya que una película puede estar
	 * disponible en diferentes sucursales
	 * 
	 * */
	
	@Id
	@NotNull
	@Column(name="id_movie_branch")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@JsonBackReference
	@JoinColumn(name = "movie_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Movie movie;
	
	@JsonBackReference
	@JoinColumn(name = "branchoffice_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private BranchOffice branchoffice;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public BranchOffice getBranchoffice() {
		return branchoffice;
	}

	public void setBranchoffice(BranchOffice branchoffice) {
		this.branchoffice = branchoffice;
	}

	@Override
	public String toString() {
		return "Movie_BranchOffice [id=" + id + ", movie=" + movie + ", branchoffice=" + branchoffice + "]";
	}

	

}
