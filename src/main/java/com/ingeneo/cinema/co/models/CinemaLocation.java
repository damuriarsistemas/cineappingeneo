package com.ingeneo.cinema.co.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.ingeneo.cinema.co.global.Constantes;
import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="cinemalocation")
public class CinemaLocation {
	
	/*id único de la tabla usado para clave primaria*/
	@Id
	@NotNull
	@Column(name="id_cinema_location")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	@JsonBackReference
	@JoinColumn(name = "cinema_id", nullable = false)
	@ManyToOne(fetch = FetchType.EAGER)
 	private Cinema cinema;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Column(name="codelocation",length=3,columnDefinition="VARCHAR(3) NOT NULL")
	private String codeLocation;
	
	@Column(name="statusLocation",length=10)
	private String statusLocation;
	
	public CinemaLocation() {
		this.statusLocation = Constantes.DISPONIBLE;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Cinema getCinema() {
		return cinema;
	}

	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}

	public String getCodeLocation() {
		return codeLocation;
	}

	public void setCodeLocation(String codeLocation) {
		this.codeLocation = codeLocation;
	}

	public String getStatusLocation() {
		return statusLocation;
	}

	public void setStatusLocation(String statusLocation) {
		this.statusLocation = statusLocation;
	}
	
	

	@Override
	public String toString() {
		return "CinemaLocation [id=" + id + ", cinema=" + cinema + ", codeLocation=" + codeLocation
				+ ", statusLocation=" + statusLocation + "]";
	}
	
	
	

}
