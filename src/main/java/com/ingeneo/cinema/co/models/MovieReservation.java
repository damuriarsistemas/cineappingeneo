package com.ingeneo.cinema.co.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ingeneo.cinema.co.global.Constantes;
import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="moviereservation")
public class MovieReservation {

	@Id
	@NotNull
	@Column(name="id_moviereservation")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@JoinColumn(name = "city_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private ScheduleMovie scheduleMovie;
	
	@JoinColumn(name = "cinemalocation_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private CinemaLocation cinemalocation;
	
	@NotNull
	@Column(name="statuspay",length=10,columnDefinition="VARCHAR(10) NOT NULL")
	private String statusPay;

	
	public MovieReservation() {
		super();
		this.statusPay = Constantes.PENDIENTE;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public ScheduleMovie getScheduleMovie() {
		return scheduleMovie;
	}


	public void setScheduleMovie(ScheduleMovie scheduleMovie) {
		this.scheduleMovie = scheduleMovie;
	}


	public CinemaLocation getCinemalocation() {
		return cinemalocation;
	}


	public void setCinemalocation(CinemaLocation cinemalocation) {
		this.cinemalocation = cinemalocation;
	}


	public String getStatusPay() {
		return statusPay;
	}


	public void setStatusPay(String statusPay) {
		this.statusPay = statusPay;
	}


	@Override
	public String toString() {
		return "MovieReservation [id=" + id + ", scheduleMovie=" + scheduleMovie + ", cinemalocation=" + cinemalocation
				+ ", statusPay=" + statusPay + "]";
	}

	
	
	
}
