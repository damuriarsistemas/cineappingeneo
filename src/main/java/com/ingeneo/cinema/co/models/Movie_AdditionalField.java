package com.ingeneo.cinema.co.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="movie_addfield")
public class Movie_AdditionalField {
	/*
	 * contiene la relación de películas con géneros, ya que una película puede tener varios
	 * géneros 
	 * 
	 * */
	
	@Id
	@NotNull
	@Column(name="id_movie_addfield")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@JsonBackReference
	@JoinColumn(name = "movie_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Movie movie;
	
	@JoinColumn(name = "addfield_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private AdditionalField additionalField;
	
	@Column(name="valor", nullable=false , columnDefinition="varchar(40) NOT NULL" , length=60)
	private String valor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public AdditionalField getAdditionalField() {
		return additionalField;
	}

	public void setAdditionalField(AdditionalField additionalField) {
		this.additionalField = additionalField;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "Movie_AdditionalField [id=" + id + ", movie=" + movie + ", additionalField=" + additionalField
				+ ", valor=" + valor + "]";
	}
	
}
