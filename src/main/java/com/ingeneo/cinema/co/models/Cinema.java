package com.ingeneo.cinema.co.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="cinema")
public class Cinema {
	
	/*id único de la tabla usado para clave primaria*/
	@Id
	@NotNull
	@Column(name="id_cinema")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	/*nombre de la sala, ejemplo: Sala - 1*/
	@NotNull
	@Column(name="descrip", nullable=false , columnDefinition="varchar(12) NOT NULL" , length=12)
	private String descripCinema;

	/*sucursal a la cual esta asociada la sala de cine*/
	@JsonBackReference
	@JoinColumn(name = "branchoffice_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private BranchOffice branchOffice;
	
	/*tipo de sala de cine*/
	@JoinColumn(name = "typecinema_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private TypeCinema typeCinema;
	
	/*cantidad de filas de la sala*/
	@NotNull
	private Integer numberRowXcinema;
	
	
	/*número de sillas por fila*/
	@NotNull
    private Integer numberChairXRow;

	@OneToMany(mappedBy="cinema", fetch = FetchType.EAGER)
	private List<CinemaLocation> cinemaLocation;

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getDescripCinema() {
		return descripCinema;
	}


	public void setDescripCinema(String descripCinema) {
		this.descripCinema = descripCinema;
	}


	public BranchOffice getBranchOffice() {
		return branchOffice;
	}


	public void setBranchOffice(BranchOffice branchOffice) {
		this.branchOffice = branchOffice;
	}


	public TypeCinema getTypeCinema() {
		return typeCinema;
	}


	public void setTypeCinema(TypeCinema typeCinema) {
		this.typeCinema = typeCinema;
	}


	public Integer getNumberRowXcinema() {
		return numberRowXcinema;
	}


	public void setNumberRowXcinema(Integer numberRowXcinema) {
		this.numberRowXcinema = numberRowXcinema;
	}


	public Integer getNumberChairXRow() {
		return numberChairXRow;
	}


	public void setNumberChairXRow(Integer numberChairXRow) {
		this.numberChairXRow = numberChairXRow;
	}
	
	

	/*
	public void addCinemaLocation(CinemaLocation obj){
        if(this.lstcinemaLocation == null){
            this.lstcinemaLocation = new ArrayList<>();
        }
        
        this.lstcinemaLocation.add(obj);
	}*/


	public List<CinemaLocation> getCinemaLocation() {
		return cinemaLocation;
	}


	public void setCinemaLocation(List<CinemaLocation> cinemaLocation) {
		this.cinemaLocation = cinemaLocation;
	}


	@Override
	public String toString() {
		return "Cinema [id=" + id + ", descripCinema=" + descripCinema + ", branchOffice=" + branchOffice
				+ ", typeCinema=" + typeCinema + ", numberRowXcinema=" + numberRowXcinema + ", numberChairXRow="
				+ numberChairXRow + "]";
	}


	

}
