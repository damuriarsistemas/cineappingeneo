package com.ingeneo.cinema.co.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;

@Entity
@Table(schema="public", name="movie")
public class Movie {
	
	/*id único de la tabla usado para clave primaria*/
	@Id
	@NotNull
	@Column(name="id_movie")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	@Size(min=1, max=35)
	@Column(name="originalname", nullable=false , columnDefinition="varchar(60) NOT NULL" , length=60)
	private String originalName;
	
	@NotNull
	@Size(min=1, max=35)
	@Column(name="translationname", nullable=false , columnDefinition="varchar(60) NOT NULL" , length=60)
	private String translationName;

	@NotNull
	private Date releaseDate;
	
	private String urlimg;
	
	//fecha de baja de la pelicula
	@NotNull
	private Date dischargeDate;
	
	@Transient
	@JsonManagedReference
	@OneToMany(mappedBy="movie", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Movie_Genre> lstmoviegenre;
	
	@Transient
	@JsonManagedReference
	@OneToMany(mappedBy="branchoffice", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Movie_BranchOffice> lstmoviebranch;
	
	@Transient
	@JsonManagedReference
	@OneToMany(mappedBy="movie", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Movie_AdditionalField> lstmovieAdditionalField;
	
	@JoinColumn(name = "typecinema_id", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private TypeCinema typeCinema;
	
	@NotNull
	private Integer duration;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getTranslationName() {
		return translationName;
	}

	public void setTranslationName(String translationName) {
		this.translationName = translationName;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getUrlimg() {
		return urlimg;
	}

	public void setUrlimg(String urlimg) {
		this.urlimg = urlimg;
	}

	public Date getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public List<Movie_Genre> getLstmoviegenre() {
		return lstmoviegenre;
	}

	public void setLstmoviegenre(List<Movie_Genre> lstmoviegenre) {
		this.lstmoviegenre = lstmoviegenre;
	}
	
	public List<Movie_BranchOffice> getLstmoviebranch() {
		return lstmoviebranch;
	}

	public void setLstmoviebranch(List<Movie_BranchOffice> lstmoviebranch) {
		this.lstmoviebranch = lstmoviebranch;
	}

	public List<Movie_AdditionalField> getLstmovieAdditionalField() {
		return lstmovieAdditionalField;
	}

	public void setLstmovieAdditionalField(List<Movie_AdditionalField> lstmovieAdditionalField) {
		this.lstmovieAdditionalField = lstmovieAdditionalField;
	}

	public void addMovie_Genre(Movie_Genre obj){
        if(this.lstmoviegenre == null){
            this.lstmoviegenre = new ArrayList<>();
        }       
        this.lstmoviegenre.add(obj);
	}
	
	public void addMovie_BranchOffice(Movie_BranchOffice obj){
        if(this.lstmoviebranch == null){
            this.lstmoviebranch = new ArrayList<>();
        }       
        this.lstmoviebranch.add(obj);
	}
	
	public void addMovie_AdditionalField(Movie_AdditionalField obj){
        if(this.lstmovieAdditionalField == null){
            this.lstmovieAdditionalField = new ArrayList<>();
        }       
        this.lstmovieAdditionalField.add(obj);
	}

	
	public TypeCinema getTypeCinema() {
		return typeCinema;
	}

	public void setTypeCinema(TypeCinema typeCinema) {
		this.typeCinema = typeCinema;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", originalName=" + originalName + ", translationName=" + translationName
				+ ", releaseDate=" + releaseDate + ", urlimg=" + urlimg + ", dischargeDate=" + dischargeDate
				+ ", lstmoviegenre=" + lstmoviegenre + ", typeCinema=" + typeCinema + ", duration=" + duration + "]";
	}

	

}
