package com.ingeneo.cinema.co.request;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model MovieRequest")
public class MovieRequest {
	
	@ApiModelProperty(value = "Id de la película", required = false)
	private Integer id;
	
	@NotEmpty(message="Debe especificarse un nombre original para la película")
	@ApiModelProperty(value = "Nombre original", required = true)
	private String originalName;
	
	@NotEmpty(message="Debe especificarse un nombre traducido al idioma local para la película")
	@ApiModelProperty(value = "Nombre traducido", required = true)
	private String translationName;

	@NotEmpty(message="Debe especificarse la fecha de lanzamiento o estreno")
	@ApiModelProperty(value = "Fecha de estreno", required = true)
	private Date releaseDate;
	
	@NotEmpty(message="Debe indicar una imagen para la portada")
	@ApiModelProperty(value = "Imagen de portada", required = true)
	private String urlimg;
	
	//fecha de baja de la pelicula
	@NotEmpty(message="Debe indicar la fecha de baja de la película")
	@ApiModelProperty(value = "Fecha de baja", required = true)
	private Date dischargeDate;
	
	/*
	@NotEmpty(message="Debe especificar un género")
	@ApiModelProperty(value = "Id del Género", required = true)
	private Integer genre_id;
	*/
	
	@NotEmpty(message="Debe especificar un formato de pélicula")
	@ApiModelProperty(value = "Id del Formato de película", required = true)
	private Integer typeCinema_id;
	
	@NotEmpty(message="Debe especificar la duración en minutos")
	@ApiModelProperty(value = "Duración en minutos", required = true)
	private Integer duration;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getTranslationName() {
		return translationName;
	}

	public void setTranslationName(String translationName) {
		this.translationName = translationName;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getUrlimg() {
		return urlimg;
	}

	public void setUrlimg(String urlimg) {
		this.urlimg = urlimg;
	}

	public Date getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	/*
	public Integer getGenre_id() {
		return genre_id;
	}

	public void setGenre_id(Integer genre_id) {
		this.genre_id = genre_id;
	}
	*/

	public Integer getTypeCinema_id() {
		return typeCinema_id;
	}

	public void setTypeCinema_id(Integer typeCinema_id) {
		this.typeCinema_id = typeCinema_id;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	
	

}
