package com.ingeneo.cinema.co.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model BranchOfficeRequest")
public class BranchOfficeRequest {

    @ApiModelProperty(value = "Id de la sucursal", required = false, notes="Solo se requiere cuando se desea hacer una actualización de alguna sucursal")
	private Integer id;

    @ApiModelProperty(value = "Nombre de la sucursal", required = true)
	@NotEmpty(message="Nombre de Sucursal no puede estar vacia")
	private String nameBranchOffice;

    @ApiModelProperty(value = "Dirección de la sucursal", required = true)
	@NotEmpty(message="Dirección no puede estar vacia")
	private String address;
	
    @ApiModelProperty(value = "Id del usuario que tiene rol administrador de la sucursal", required = true)
	@NotNull(message="Debe indicar el id del administrador responsable de sucursal")
	private Integer user_manager_id;
	
    @ApiModelProperty(value = "Id de la ciudad a la que estará asociada la sucursal", required = true)
	@NotNull(message="Debe indicar el id de ciudad asociada")
	private Integer city_id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameBranchOffice() {
		return nameBranchOffice;
	}

	public void setNameBranchOffice(String nameBranchOffice) {
		this.nameBranchOffice = nameBranchOffice;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getUser_manager_id() {
		return user_manager_id;
	}

	public void setUser_manager_id(Integer user_manager_id) {
		this.user_manager_id = user_manager_id;
	}

	public Integer getCity_id() {
		return city_id;
	}

	public void setCity_id(Integer city_id) {
		this.city_id = city_id;
	}
	
		

	

}
