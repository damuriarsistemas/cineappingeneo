package com.ingeneo.cinema.co.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model CinemaParamRequest")
public class CinemaParamRequest {
	
	
	@NotNull
    @Min(value = 3 , message = "El valor mínimo debe ser 3")
    @Max(value = 27 , message = "El valor máximo debería ser de 27")
	@ApiModelProperty(value = "Número de filas máxima de la sala de cine", required = true)
	private Integer numberRowXcinema;
	
	
	@NotNull
    @Min(value = 3 , message = "El valor mínimo debe ser 3")
    @Max(value = 10 , message = "El valor máximo debería ser de 10")
	@ApiModelProperty(value = "Número de filas silla por fila de la sala de cine", required = true)
    private Integer numberChairXRow;


	public Integer getNumberRowXcinema() {
		return numberRowXcinema;
	}


	public void setNumberRowXcinema(Integer numberRowXcinema) {
		this.numberRowXcinema = numberRowXcinema;
	}


	public Integer getNumberChairXRow() {
		return numberChairXRow;
	}


	public void setNumberChairXRow(Integer numberChairXRow) {
		this.numberChairXRow = numberChairXRow;
	}

	

}
