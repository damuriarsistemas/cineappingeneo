package com.ingeneo.cinema.co.request;


import javax.validation.constraints.NotEmpty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model Movie_BranchOfficeRequest")
public class Movie_BranchOfficeRequest {
	
	@ApiModelProperty(value = "Id de registro", required = false)
	private Integer id;
	
	@NotEmpty(message="Debe especificarse un id de película")
	@ApiModelProperty(value = "Id de película", required = true)
	private Integer movie_id;
	
	@NotEmpty(message="Debe especificarse un id de sucursal")
	@ApiModelProperty(value = "Id de género", required = true)
	private Integer branchOffice_id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMovie_id() {
		return movie_id;
	}

	public void setMovie_id(Integer movie_id) {
		this.movie_id = movie_id;
	}

	public Integer getBranchOffice_id() {
		return branchOffice_id;
	}

	public void setBranchOffice_id(Integer branchOffice_id) {
		this.branchOffice_id = branchOffice_id;
	}

	

	
}
