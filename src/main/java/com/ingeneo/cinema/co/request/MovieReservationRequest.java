package com.ingeneo.cinema.co.request;

import javax.validation.constraints.NotNull;

import com.ingeneo.cinema.co.global.Constantes;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model MovieReservationRequest")
public class MovieReservationRequest {

	@ApiModelProperty(value = "Id de la película", required = false)
	private Integer id;
	
	@NotNull
	@ApiModelProperty(value = "Id de programación de película", required = true)
	private Integer scheduleMovie_id;
	
	@NotNull
	@ApiModelProperty(value = "Id del puesto en la sala de cine", required = true)
	private Integer cinemaLocation_id;
	
	@NotNull
	@ApiModelProperty(value = "Estatus del pago de la reservación", required = true)
	private String statusPay;

	
	public MovieReservationRequest() {
		super();
		this.statusPay = Constantes.PENDIENTE;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getScheduleMovie_id() {
		return scheduleMovie_id;
	}


	public void setScheduleMovie_id(Integer scheduleMovie_id) {
		this.scheduleMovie_id = scheduleMovie_id;
	}


	public Integer getCinemaLocation_id() {
		return cinemaLocation_id;
	}


	public void setCinemaLocation_id(Integer cinemaLocation_id) {
		this.cinemaLocation_id = cinemaLocation_id;
	}


	public String getStatusPay() {
		return statusPay;
	}


	public void setStatusPay(String statusPay) {
		this.statusPay = statusPay;
	}
	
	
}
