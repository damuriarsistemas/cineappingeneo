package com.ingeneo.cinema.co.request;


import javax.validation.constraints.NotEmpty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model Movie_GenreRequest")
public class Movie_GenreRequest {
	
	@ApiModelProperty(value = "Id de registro", required = false)
	private Integer id;
	
	@NotEmpty(message="Debe especificarse un id de película")
	@ApiModelProperty(value = "Id de película", required = true)
	private Integer movie_id;
	
	@NotEmpty(message="Debe especificarse un id de género")
	@ApiModelProperty(value = "Id de género", required = true)
	private Integer genre_id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMovie_id() {
		return movie_id;
	}

	public void setMovie_id(Integer movie_id) {
		this.movie_id = movie_id;
	}

	public Integer getGenre_id() {
		return genre_id;
	}

	public void setGenre_id(Integer genre_id) {
		this.genre_id = genre_id;
	}

	
}
