package com.ingeneo.cinema.co.request;

import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model Movie_AdditionalFieldRequest")
public class Movie_AdditionalFieldRequest {
	/*
	 * contiene la relación de películas con géneros, ya que una película puede tener varios
	 * géneros 
	 * 
	 * */
    @ApiModelProperty(value = "Id del registro", required = false, notes="Solo se requiere cuando se desea hacer una actualización de alguna sala")
	private Integer id;
	
    @NotNull
   	@ApiModelProperty(value = "Id de pelicula", required = true)
	private Integer movie_id;
	
    @NotNull
    @ApiModelProperty(value = "Id de atributo adicional a agregar a pelicula", required = true)
	private Integer additionalField_id;

    @NotNull
    @ApiModelProperty(value = "Valor o información adicional", required = true)
	private String valor;
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMovie_id() {
		return movie_id;
	}

	public void setMovie_id(Integer movie_id) {
		this.movie_id = movie_id;
	}

	public Integer getAdditionalField_id() {
		return additionalField_id;
	}

	public void setAdditionalField_id(Integer additionalField_id) {
		this.additionalField_id = additionalField_id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	

    
	
}
