package com.ingeneo.cinema.co.request;

import javax.validation.constraints.NotEmpty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model AdditionalFieldRequest")
public class AdditionalFieldRequest {
	
	
	@ApiModelProperty(value = "Id de atributo adicional para películas", required=false)
	private Integer id;
	
	@NotEmpty(message="Debe especificarse un nombre atributo")
	@ApiModelProperty(value = "Nombre del atributo adicional para películas", required = true)
	private String additionalField;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAdditionalField() {
		return additionalField;
	}

	public void setAdditionalField(String additionalField) {
		this.additionalField = additionalField;
	}

	@Override
	public String toString() {
		return "AdditionalField [id=" + id + ", additionalField=" + additionalField + "]";
	}

}
