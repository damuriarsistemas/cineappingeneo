package com.ingeneo.cinema.co.request;

import javax.validation.constraints.NotEmpty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model GenreRequest")
public class GenreRequest {
	
	/*id único de la tabla usado para clave primaria*/
    @ApiModelProperty(value = "Id del género", required = false, notes="Solo se requiere cuando se desea hacer una actualización de algún género")
    private Integer id;
	
	/*nombre del género*/
    @NotEmpty(message="Debe especificarse un nombre o tipo de género")
	@ApiModelProperty(value = "Tipo de género", required = true)
	private String genre;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTypegenre() {
		return genre;
	}

	public void setTypegenre(String typegenre) {
		this.genre = typegenre;
	}

	@Override
	public String toString() {
		return "Genre [id=" + id + ", genre=" + genre + "]";
	}

	
	
		
	

}
