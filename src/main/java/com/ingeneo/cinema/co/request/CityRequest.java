package com.ingeneo.cinema.co.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model CityRequest")
public class CityRequest {
	
	
	@NotNull
    @ApiModelProperty(value = "Id de la sucursal", required = false)
	private Integer id;
	
	@NotEmpty(message="Debe especificarse un nombre de ciudad")
	@ApiModelProperty(value = "Nombre de la ciudad", required = true)
	private String nameCity;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameCity() {
		return nameCity;
	}

	public void setNameCity(String nameCity) {
		this.nameCity = nameCity;
	}

	@Override
	public String toString() {
		return "City [id=" + id + ", nameCity=" + nameCity + "]";
	}

	
	

}
