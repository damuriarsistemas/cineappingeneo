package com.ingeneo.cinema.co.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import com.ingeneo.cinema.co.global.Constantes;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model UserRequest")
public class UserRequest {
	
    @ApiModelProperty(value = "Id del usuario", required = false, notes="Solo se requiere cuando se desea hacer una actualización de algún usuario")
	private Integer id;
	
	@NotEmpty(message="La cuenta no puede quedar vacia")
	@ApiModelProperty(value = "cuenta", required = true)
	@Size(min=4, max=12,message="Debe especificar entre 4 y 12 carácteres para la cuenta")
	private String acount;


	@Size(min=4, max=12,message="Debe especificar entre 4 y 12 carácteres para la cuenta")
	@NotEmpty(message="El password no puede quedar vacio")
	@ApiModelProperty(value = "password", required = true)
	private String password;
	
	private Integer rolcode;

	
	public UserRequest() {
		this.rolcode = Constantes.BASIC_USER;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAcount() {
		return acount;
	}

	public void setAcount(String acount) {
		this.acount = acount;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRolcode() {
		return rolcode;
	}

	public void setRolcode(Integer rolcode) {
		this.rolcode = rolcode;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", acount=" + acount + ", password=" + password + ", rolcode=" + rolcode + "]";
	}
	
	

}
