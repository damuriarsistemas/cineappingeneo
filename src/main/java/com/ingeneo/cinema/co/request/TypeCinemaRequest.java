package com.ingeneo.cinema.co.request;

import javax.validation.constraints.NotEmpty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel("Model TypeCinema")
public class TypeCinemaRequest {
	
	/*id único de la tabla usado para clave primaria*/
    @ApiModelProperty(value = "Id del tipo de sala", required = false, notes="Solo se requiere cuando se desea hacer una actualización de alguna sucursal")
	private Integer id;
	
	/*nombre del tipo de sala*/
    @NotEmpty(message="Debe especificarse un nombre de tipo de sala")
	@ApiModelProperty(value = "Nombre del tipo de sala", required = true)
	private String typeCinema;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTypeCinema() {
		return typeCinema;
	}

	public void setTypeCinema(String typeCinema) {
		this.typeCinema = typeCinema;
	}

	@Override
	public String toString() {
		return "TypeCinema [id=" + id + ", typeCinema=" + typeCinema + "]";
	}

	
		
	

}
