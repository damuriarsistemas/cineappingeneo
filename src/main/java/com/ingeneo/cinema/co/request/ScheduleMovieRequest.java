package com.ingeneo.cinema.co.request;


import javax.validation.constraints.NotEmpty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model ScheduleMovieRequest")
public class ScheduleMovieRequest {

	@ApiModelProperty(value = "Id del registro de programación", required = true)
	private Integer id;
	
	@NotEmpty(message="Debe indicar el id de una sucursal")
	@ApiModelProperty(value = "Id de la sucursal", required = true)
	private Integer branchOffice_id;
	
	@NotEmpty(message="Debe indicar el id de una película")
	@ApiModelProperty(value = "Id de la película", required = true)
	private Integer movie_id;
	
	@NotEmpty(message="Debe indicar el id de una sala de cine")
	@ApiModelProperty(value = "Id de sala de cine", required = true)
	private Integer cinema_id;
	
	@NotEmpty(message="Debe indicar la fecha de presentación")
	@ApiModelProperty(value = "Fecha de presentación de la película formato dd-mm-yyyy", required = true)
	private String fecha;
	
	@NotEmpty(message="Debe indicar la hora de presentación")
	@ApiModelProperty(value = "Hora de presentación de la película", required = true)
	private String hora;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBranchOffice_id() {
		return branchOffice_id;
	}

	public void setBranchOffice_id(Integer branchOffice_id) {
		this.branchOffice_id = branchOffice_id;
	}

	public Integer getMovie_id() {
		return movie_id;
	}

	public void setMovie_id(Integer movie_id) {
		this.movie_id = movie_id;
	}

	public Integer getCinema_id() {
		return cinema_id;
	}

	public void setCinema_id(Integer cinema_id) {
		this.cinema_id = cinema_id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	
}
