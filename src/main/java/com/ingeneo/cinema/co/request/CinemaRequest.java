package com.ingeneo.cinema.co.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model CinemaRequest")
public class CinemaRequest {
	
	/*id único de la tabla usado para clave primaria*/
    @ApiModelProperty(value = "Id de la sala de cine", required = false, notes="Solo se requiere cuando se desea hacer una actualización de alguna sala")
	private Integer id;
	
	/*nombre de la sala, ejemplo: Sala - 1*/
	@NotNull
	@ApiModelProperty(value = "Nombre de la sala de cine", required = true)
	private String descripCinema;

	@ApiModelProperty(value = "Id de la sucursal a la que esta asociada", required = true)
	private Integer branchOffice_id;
	
	/*tipo de sala de cine*/
	@ApiModelProperty(value = "Id del tipo de sala de cine", required = true)
	private Integer typeCinema_id;
	
	/*cantidad de filas de la sala*/
	@NotNull
    @Min(value = 3 , message = "El valor mínimo debe ser 3")
    @Max(value = 27 , message = "El valor máximo debería ser de 27")
	@ApiModelProperty(value = "Número de filas máxima de la sala de cine", required = true)
	private Integer numberRowXcinema;
	
	
	/*número de sillas por fila*/
	@NotNull
    @Min(value = 3 , message = "El valor mínimo debe ser 3")
    @Max(value = 10 , message = "El valor máximo debería ser de 10")
	@ApiModelProperty(value = "Número de filas silla por fila de la sala de cine", required = true)
    private Integer numberChairXRow;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getDescripCinema() {
		return descripCinema;
	}


	public void setDescripCinema(String descripCinema) {
		this.descripCinema = descripCinema;
	}


	public Integer getBranchOffice_id() {
		return branchOffice_id;
	}


	public void setBranchOffice_id(Integer branchOffice_id) {
		this.branchOffice_id = branchOffice_id;
	}


	public Integer getTypeCinema_id() {
		return typeCinema_id;
	}


	public void setTypeCinema_id(Integer typeCinema_id) {
		this.typeCinema_id = typeCinema_id;
	}


	public Integer getNumberRowXcinema() {
		return numberRowXcinema;
	}


	public void setNumberRowXcinema(Integer numberRowXcinema) {
		this.numberRowXcinema = numberRowXcinema;
	}


	public Integer getNumberChairXRow() {
		return numberChairXRow;
	}


	public void setNumberChairXRow(Integer numberChairXRow) {
		this.numberChairXRow = numberChairXRow;
	}


    
	

}
