package com.ingeneo.cinema.co.request;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model CinemaLocationRequest")
public class CinemaLocationRequest {
	
	/*id único de la tabla usado para clave primaria*/
	@ApiModelProperty(value = "Id del puesto dentro de la sala", required = true)
	private Integer id;
	
	@NotNull
	@ApiModelProperty(value = "Id de la sala de cine", required = true)
	private Integer cinema_id;
	
	@NotEmpty
	@ApiModelProperty(value = "Código del puesto dentro de la sala", required = true)
	private String codeLocation;
	
	@NotEmpty
	@ApiModelProperty(value = "Estatus del puesto dentro de la sala", required = true)
	private String statusLocation;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCinema_id() {
		return cinema_id;
	}

	public void setCinema_id(Integer cinema_id) {
		this.cinema_id = cinema_id;
	}

	public String getCodeLocation() {
		return codeLocation;
	}

	public void setCodeLocation(String codeLocation) {
		this.codeLocation = codeLocation;
	}

	public String getStatusLocation() {
		return statusLocation;
	}

	public void setStatusLocation(String statusLocation) {
		this.statusLocation = statusLocation;
	}	
	

}
