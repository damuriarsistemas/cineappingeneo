package com.ingeneo.cinema.co.reponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ingeneo.cinema.co.global.Constantes;

public class CinemaLocationResponse {
	
	
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String codeLocation;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String statusLocation;
	
	public CinemaLocationResponse() {
		super();
		this.statusLocation = Constantes.DISPONIBLE;
	}

	public String getCodeLocation() {
		return codeLocation;
	}

	public void setCodeLocation(String codeLocation) {
		this.codeLocation = codeLocation;
	}

	public String getStatusLocation() {
		return statusLocation;
	}

	public void setStatusLocation(String statusLocation) {
		this.statusLocation = statusLocation;
	}


}
