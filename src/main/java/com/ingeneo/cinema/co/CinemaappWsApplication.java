package com.ingeneo.cinema.co;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemaappWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CinemaappWsApplication.class, args);
	}

}
