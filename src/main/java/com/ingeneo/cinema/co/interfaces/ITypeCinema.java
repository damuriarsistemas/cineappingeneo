package com.ingeneo.cinema.co.interfaces;

import java.util.List;
import com.ingeneo.cinema.co.models.TypeCinema;

public interface ITypeCinema {

	List<TypeCinema> desplegarTodos();
	TypeCinema guardar(TypeCinema typecinema);
	TypeCinema getTypeCinemaById(Integer id);
	void eliminar(Integer id);
	TypeCinema actualizar(TypeCinema typecinema);
	
}
