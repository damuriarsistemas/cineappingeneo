package com.ingeneo.cinema.co.interfaces;

import java.util.List;
import com.ingeneo.cinema.co.models.BranchOffice;


public interface IBranchOffice {

	List<BranchOffice> desplegarTodos();
	BranchOffice guardar(BranchOffice branchoffice);
	BranchOffice getBranchById(Integer id);
	void eliminar(Integer id);
	BranchOffice actualizar(BranchOffice branchoffice);
	
}
