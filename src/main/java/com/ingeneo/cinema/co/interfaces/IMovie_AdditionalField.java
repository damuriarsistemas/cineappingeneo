package com.ingeneo.cinema.co.interfaces;

import com.ingeneo.cinema.co.models.Movie_AdditionalField;

public interface IMovie_AdditionalField {
	
	Movie_AdditionalField guardar(Movie_AdditionalField movie_AdditionalField);
	void eliminar(Integer id);

}
