package com.ingeneo.cinema.co.interfaces;
import java.util.List;

import com.ingeneo.cinema.co.models.Movie_BranchOffice;

public interface IMovie_BranchOffice {
	
	List<Movie_BranchOffice> desplegarTodos();
	Movie_BranchOffice guardar(Movie_BranchOffice movie_BranchOffice);
	void eliminar(Integer id);

}
