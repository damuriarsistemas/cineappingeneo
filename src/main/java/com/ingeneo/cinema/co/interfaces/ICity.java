package com.ingeneo.cinema.co.interfaces;

import java.util.List;
import com.ingeneo.cinema.co.models.City;

public interface ICity {

	List<City> desplegarTodos();
	City guardar(City city);
	City getCityById(Integer id);
	void eliminar(Integer id);
	City actualizar(City city);
	
}
