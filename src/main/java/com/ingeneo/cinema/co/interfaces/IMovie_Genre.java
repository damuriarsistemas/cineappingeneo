package com.ingeneo.cinema.co.interfaces;

import com.ingeneo.cinema.co.models.Movie_Genre;

public interface IMovie_Genre {
	
	Movie_Genre guardar(Movie_Genre movie_Genre);
	void eliminar(Integer id);

}
