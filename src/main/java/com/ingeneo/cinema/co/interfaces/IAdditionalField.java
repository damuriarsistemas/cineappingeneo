package com.ingeneo.cinema.co.interfaces;

import java.util.List;

import com.ingeneo.cinema.co.models.AdditionalField;

public interface IAdditionalField {
	
	List<AdditionalField> desplegarTodos();
	AdditionalField guardar(AdditionalField additionalField);
	void eliminar(Integer id);
	AdditionalField getAdditionalFieldById(Integer id);

}
