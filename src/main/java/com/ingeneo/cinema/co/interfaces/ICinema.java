package com.ingeneo.cinema.co.interfaces;

import java.util.List;
import com.ingeneo.cinema.co.models.Cinema;

public interface ICinema {

	List<Cinema> desplegarTodos();
	Cinema guardar(Cinema cinema);
	Cinema getCinemaById(Integer id);
	void eliminar(Integer id);
	Cinema actualizar(Cinema cinema);
	
}
