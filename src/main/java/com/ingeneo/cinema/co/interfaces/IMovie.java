package com.ingeneo.cinema.co.interfaces;

import java.util.List;
import com.ingeneo.cinema.co.models.Movie;

public interface IMovie {

	List<Movie> desplegarTodos();
	Movie guardar(Movie movie);
	Movie getMovieById(Integer id);
	void eliminar(Integer id);
	Movie actualizar(Movie movie);
	
}
