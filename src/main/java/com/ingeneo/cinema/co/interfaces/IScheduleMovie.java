package com.ingeneo.cinema.co.interfaces;

import java.util.List;

import com.ingeneo.cinema.co.models.ScheduleMovie;


public interface IScheduleMovie {

	List<ScheduleMovie> desplegarTodos();
	ScheduleMovie guardar(ScheduleMovie scheduleMovie);
	ScheduleMovie getScheduleById(Integer id);
	void eliminar(Integer id);
	ScheduleMovie actualizar(ScheduleMovie scheduleMovie);
	
}
