package com.ingeneo.cinema.co.interfaces;

import java.util.List;
import com.ingeneo.cinema.co.models.Genre;

public interface IGenre {

	List<Genre> desplegarTodos();
	Genre guardar(Genre genre);
	Genre getGenreById(Integer id);
	void eliminar(Integer id);
	Genre actualizar(Genre genre);
	
}
