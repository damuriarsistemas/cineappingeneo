package com.ingeneo.cinema.co.interfaces;

import java.util.List;
import com.ingeneo.cinema.co.models.User;

public interface IUser {

	List<User> desplegarTodos();
	User guardar(User user);
	User getUserById(Integer id);
	void eliminar(Integer id);
	User actualizar(User user);
}
