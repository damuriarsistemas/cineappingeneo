package com.ingeneo.cinema.co.interfaces;

import java.util.List;
import com.ingeneo.cinema.co.models.MovieReservation;

public interface IMovieReservation {
	
	List<MovieReservation> desplegarTodos();
	MovieReservation guardar(MovieReservation movieReservation);
	MovieReservation getBranchById(Integer id);
	void eliminar(Integer id);
	MovieReservation actualizar(MovieReservation movieReservation);


}
