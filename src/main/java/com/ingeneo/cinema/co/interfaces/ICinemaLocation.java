package com.ingeneo.cinema.co.interfaces;

import java.util.List;
import com.ingeneo.cinema.co.models.CinemaLocation;

public interface ICinemaLocation {

	List<CinemaLocation> desplegarTodos();
	CinemaLocation  guardar(CinemaLocation cinemaLocation);
	CinemaLocation  getCinemaLocationById(Integer id);
	void eliminar(Integer id);
	CinemaLocation actualizar(CinemaLocation cinemaLocation);

}
