package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.IScheduleMovie;
import com.ingeneo.cinema.co.models.ScheduleMovie;
import com.ingeneo.cinema.co.repository.ScheduleMovieRepository;

@Service
public class ScheduleMovieJPA implements IScheduleMovie{

	@Autowired
	ScheduleMovieRepository repo;
	
	@Override
	public List<ScheduleMovie> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public ScheduleMovie guardar(ScheduleMovie scheduleMovie) {
		// TODO Auto-generated method stub
		return repo.save(scheduleMovie);
	}

	@Override
	public ScheduleMovie getScheduleById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

	@Override
	public ScheduleMovie actualizar(ScheduleMovie scheduleMovie) {
		// TODO Auto-generated method stub
		return repo.save(scheduleMovie);
	}

}
