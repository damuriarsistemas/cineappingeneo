package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.IGenre;
import com.ingeneo.cinema.co.models.Genre;
import com.ingeneo.cinema.co.repository.GenreRepository;

@Service
public class GenreJPA implements IGenre{
	
	@Autowired
	GenreRepository repo;

	@Override
	public List<Genre> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Genre guardar(Genre genre) {
		// TODO Auto-generated method stub
		return repo.save(genre);
		
	}

	@Override
	public Genre getGenreById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
	}

	@Override
	public Genre actualizar(Genre genre) {
		// TODO Auto-generated method stub
		return repo.save(genre);
	}

}
