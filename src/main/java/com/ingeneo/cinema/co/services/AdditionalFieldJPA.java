package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.IAdditionalField;
import com.ingeneo.cinema.co.models.AdditionalField;
import com.ingeneo.cinema.co.repository.AdditionalFieldRepository;

@Service
public class AdditionalFieldJPA implements IAdditionalField{

	@Autowired
	AdditionalFieldRepository repo;
	
	@Override
	public List<AdditionalField> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public AdditionalField guardar(AdditionalField additionalField) {
		// TODO Auto-generated method stub
		return repo.save(additionalField);
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

	@Override
	public AdditionalField getAdditionalFieldById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

}
