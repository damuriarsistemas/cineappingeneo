package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.IMovie_BranchOffice;
import com.ingeneo.cinema.co.models.Movie_BranchOffice;
import com.ingeneo.cinema.co.repository.Movie_BranchOfficeRepository;

@Service
public class Movie_BranchOfficeJPA implements IMovie_BranchOffice{

	@Autowired
	Movie_BranchOfficeRepository repo;
	
	@Override
	public Movie_BranchOffice guardar(Movie_BranchOffice movie_BranchOffice) {
		// TODO Auto-generated method stub
		return repo.save(movie_BranchOffice);
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

	@Override
	public List<Movie_BranchOffice> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

}
