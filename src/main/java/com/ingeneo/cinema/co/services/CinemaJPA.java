package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.ICinema;
import com.ingeneo.cinema.co.models.Cinema;
import com.ingeneo.cinema.co.repository.CinemaRepository;

@Service
public class CinemaJPA implements ICinema {

	@Autowired
	CinemaRepository repo;
	
	@Override
	public List<Cinema> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Cinema guardar(Cinema cinema) {
		// TODO Auto-generated method stub
		return repo.save(cinema);
		
		
	}

	@Override
	public Cinema getCinemaById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

	@Override
	public Cinema actualizar(Cinema cinema) {
		// TODO Auto-generated method stub
		return repo.save(cinema);
		
	}

}
