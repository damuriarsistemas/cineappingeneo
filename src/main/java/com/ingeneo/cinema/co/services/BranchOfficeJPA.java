package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.IBranchOffice;
import com.ingeneo.cinema.co.models.BranchOffice;
import com.ingeneo.cinema.co.repository.BranchOfficeRepository;

@Service
public class BranchOfficeJPA implements IBranchOffice{

	@Autowired
	BranchOfficeRepository repo;
	
	@Override
	public List<BranchOffice> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public BranchOffice guardar(BranchOffice branchoffice){
		
		return repo.save(branchoffice);
		
	}

	@Override
	public BranchOffice getBranchById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

	@Override
	public BranchOffice actualizar(BranchOffice branchoffice) {
		// TODO Auto-generated method stub
		return repo.save(branchoffice);
		
	}

}
