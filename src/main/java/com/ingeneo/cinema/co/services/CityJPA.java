package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.ICity;
import com.ingeneo.cinema.co.models.City;
import com.ingeneo.cinema.co.repository.CityRepository;

@Service
public class CityJPA implements ICity{
	
	
	@Autowired
	CityRepository repo;

	@Override
	public List<City> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public City guardar(City city) {
		// TODO Auto-generated method stub
		return repo.save(city);
		
	}

	@Override
	public City getCityById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

	@Override
	public City actualizar(City city) {
		// TODO Auto-generated method stub
		return repo.save(city);
	}
	

}
