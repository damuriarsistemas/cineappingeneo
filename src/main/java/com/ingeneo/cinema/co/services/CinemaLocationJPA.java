package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.ICinemaLocation;
import com.ingeneo.cinema.co.models.CinemaLocation;
import com.ingeneo.cinema.co.repository.CinemaLocationRepository;

@Service
public class CinemaLocationJPA implements ICinemaLocation {

	@Autowired
	CinemaLocationRepository repo;
	
	@Override
	public List<CinemaLocation> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public CinemaLocation guardar(CinemaLocation cinemaLocation) {
		// TODO Auto-generated method stub
		return repo.save(cinemaLocation);
	}

	@Override
	public CinemaLocation getCinemaLocationById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
	}

	@Override
	public CinemaLocation actualizar(CinemaLocation cinemaLocation) {
		// TODO Auto-generated method stub
		return repo.save(cinemaLocation);
	}

}
