package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.IMovieReservation;
import com.ingeneo.cinema.co.models.MovieReservation;
import com.ingeneo.cinema.co.repository.MovieReservationRepository;

@Service
public class MovieReservationJPA implements IMovieReservation {

	@Autowired
	MovieReservationRepository repo;
	
	@Override
	public List<MovieReservation> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public MovieReservation guardar(MovieReservation movieReservation) {
		// TODO Auto-generated method stub
		return repo.save(movieReservation);
	}

	@Override
	public MovieReservation getBranchById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

	@Override
	public MovieReservation actualizar(MovieReservation movieReservation) {
		// TODO Auto-generated method stub
		return repo.save(movieReservation);
	}

}
