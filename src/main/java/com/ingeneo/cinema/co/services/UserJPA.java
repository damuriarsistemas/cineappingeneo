package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.IUser;
import com.ingeneo.cinema.co.models.User;
import com.ingeneo.cinema.co.repository.UserRepository;

@Service
public class UserJPA implements IUser {

	
	@Autowired
	UserRepository repo;
	
	@Override
	public List<User> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public User guardar(User user) {
		// TODO Auto-generated method stub
		return repo.save(user);
		
	}

	@Override
	public User getUserById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

	@Override
	public User actualizar(User user) {
		// TODO Auto-generated method stub
		return repo.save(user);	
	}

}
