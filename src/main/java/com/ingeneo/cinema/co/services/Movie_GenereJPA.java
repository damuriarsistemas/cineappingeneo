package com.ingeneo.cinema.co.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ingeneo.cinema.co.interfaces.IMovie_Genre;
import com.ingeneo.cinema.co.models.Movie_Genre;
import com.ingeneo.cinema.co.repository.Movie_GenreRepository;

@Service
public class Movie_GenereJPA implements IMovie_Genre{

	@Autowired
	Movie_GenreRepository repo;
	
	
	@Override
	public Movie_Genre guardar(Movie_Genre movie_Genre) {
		// TODO Auto-generated method stub
		return repo.save(movie_Genre);
		
	}

	
	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);		
	}

}
