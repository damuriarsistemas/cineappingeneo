package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.ITypeCinema;
import com.ingeneo.cinema.co.models.TypeCinema;
import com.ingeneo.cinema.co.repository.TypeCinemaRepository;

@Service
public class TypeCinemaJPA implements ITypeCinema{

	@Autowired
	TypeCinemaRepository repo;
	
	@Override
	public List<TypeCinema> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public TypeCinema guardar(TypeCinema typecinema) {
		// TODO Auto-generated method stub
		return repo.save(typecinema);
	}

	@Override
	public TypeCinema getTypeCinemaById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
	}

	@Override
	public TypeCinema actualizar(TypeCinema typecinema) {
		// TODO Auto-generated method stub
		return repo.save(typecinema);
	}

}
