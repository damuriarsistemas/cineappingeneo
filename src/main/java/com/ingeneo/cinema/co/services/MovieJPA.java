package com.ingeneo.cinema.co.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.IMovie;
import com.ingeneo.cinema.co.models.Movie;
import com.ingeneo.cinema.co.repository.MovieRepository;

@Service
public class MovieJPA implements IMovie{

	@Autowired
	MovieRepository repo;
	
	
	@Override
	public List<Movie> desplegarTodos() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Movie guardar(Movie movie) {
		// TODO Auto-generated method stub
		return repo.save(movie);
		
	}

	@Override
	public Movie getMovieById(Integer id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);		
	}

	@Override
	public Movie actualizar(Movie movie) {
		// TODO Auto-generated method stub
		return repo.save(movie);
		
	}

}
