package com.ingeneo.cinema.co.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingeneo.cinema.co.interfaces.IMovie_AdditionalField;
import com.ingeneo.cinema.co.models.Movie_AdditionalField;
import com.ingeneo.cinema.co.repository.Movie_AdditionalFieldRepository;

@Service
public class Movie_AdditionalFieldJPA implements IMovie_AdditionalField{

	@Autowired
	Movie_AdditionalFieldRepository repo;
	
	
	@Override
	public Movie_AdditionalField guardar(Movie_AdditionalField movie_AdditionalField) {
		// TODO Auto-generated method stub
		return repo.save(movie_AdditionalField);
	}

	@Override
	public void eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

}
