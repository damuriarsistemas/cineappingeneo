package com.ingeneo.cinema.co.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingeneo.cinema.co.models.Movie_AdditionalField;

@Repository
public interface Movie_AdditionalFieldRepository extends JpaRepository<Movie_AdditionalField, Integer> {

}
