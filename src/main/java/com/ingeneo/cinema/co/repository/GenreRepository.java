package com.ingeneo.cinema.co.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingeneo.cinema.co.models.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Integer> {

}
