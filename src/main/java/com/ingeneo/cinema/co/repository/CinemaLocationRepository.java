package com.ingeneo.cinema.co.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingeneo.cinema.co.models.CinemaLocation;

@Repository
public interface CinemaLocationRepository extends JpaRepository<CinemaLocation,Integer> {

}
