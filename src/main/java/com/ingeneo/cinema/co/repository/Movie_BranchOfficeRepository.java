package com.ingeneo.cinema.co.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingeneo.cinema.co.models.Movie_BranchOffice;

@Repository
public interface Movie_BranchOfficeRepository extends JpaRepository<Movie_BranchOffice, Integer>{

}
