package com.ingeneo.cinema.co.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingeneo.cinema.co.models.MovieReservation;

@Repository
public interface MovieReservationRepository extends JpaRepository<MovieReservation,Integer> {

}
