package com.ingeneo.cinema.co.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingeneo.cinema.co.models.Movie_Genre;

@Repository
public interface Movie_GenreRepository extends JpaRepository<Movie_Genre , Integer> {

}
