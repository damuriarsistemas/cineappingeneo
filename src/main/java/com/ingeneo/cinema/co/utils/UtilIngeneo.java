package com.ingeneo.cinema.co.utils;

import java.util.ArrayList;
import java.util.List;

import com.ingeneo.cinema.co.reponse.CinemaLocationResponse;

public class UtilIngeneo {

	public static List<CinemaLocationResponse> generarPuestos(Integer filas,Integer puestos) {
		List<CinemaLocationResponse> listaResponse = null;
		int a= 65;
		for(int i = 0; i < filas; i=i+1) {
			char k=(char) a;
			k=(char) a;
			for(int j = 1; j <= puestos; j=j+1) {
				
				CinemaLocationResponse cinemaLocationResp = new CinemaLocationResponse();
				
				String letra = Character.toString((char) k);
				cinemaLocationResp.setCodeLocation(letra.concat(String.valueOf(j)));
				
				if (listaResponse == null) {
					listaResponse = new ArrayList<>();
				}
				listaResponse.add(cinemaLocationResp);			
			}
			a=a+1;  
		}
		return listaResponse; 
		
	}
}
