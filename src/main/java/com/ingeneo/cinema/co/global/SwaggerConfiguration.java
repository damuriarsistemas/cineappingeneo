package com.ingeneo.cinema.co.global;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;



@EnableSwagger2
@Configuration
public class SwaggerConfiguration {
    /**
     * Publish a bean to generate swagger2 endpoints
     * @return a swagger configuration bean
     */
    @Bean
    public Docket cinemaWSApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(cinemaWSApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.ingeneo.cinema.co.endpoints"))
                .build();
    }

    
   private ApiInfo cinemaWSApiInfo() {

        return new ApiInfoBuilder()
                .title("Web services cinema, by Ingeneo")
                .version("1.0")
                .build();

    }

}