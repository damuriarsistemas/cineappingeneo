package com.ingeneo.cinema.co.global;

public class Constantes {

	public static final String NO_EXISTE = "No existe información para mostrar";
	public static final String CIUDAD_NO_EXISTE = "La ciudad indicada no existe";
	public static final String TIPO_SALA_CINE_NO_EXISTE = "El tipo de sala de cine indicado no existe";
	public static final String GENERO_NO_EXISTE = "El Género no existe";
	public static final String PELICULA_NO_EXISTE = "La película indicada no existe";
	public static final String USUARIO_NO_EXISTE = "El usuario indicado no existe";
	public static final String SUCURSAL_NO_EXISTE = "La sucursal indicada no existe";
	public static final String CINEMA_NO_EXISTE = "La sala de cine indicada no existe";
	public static final String CINEMA_PUESTO_NO_EXISTE = "No existe el puesto indicado en la sala de cine";
	
	public static final String ELIMINADO = "Registro eliminado";
	
	/*usaremos de manera sencilla estas constantes para manejar los únicos dos
	 * perfiles que usaremos dentro de la app*/
	public static final Integer BASIC_USER= 0;
	public static final Integer MANAGER_USER= 1;
	
	public static final String PENDIENTE = "PENDIENTE";
	public static final String PAGADO = "PAGADO";

	public static final String DISPONIBLE = "DISPONIBLE";
	public static final String RESERVADO = "RESERVADO";

}
