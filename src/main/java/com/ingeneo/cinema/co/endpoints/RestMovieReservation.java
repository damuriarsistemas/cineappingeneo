package com.ingeneo.cinema.co.endpoints;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.ingeneo.cinema.co.global.Constantes;
import com.ingeneo.cinema.co.models.CinemaLocation;
import com.ingeneo.cinema.co.models.MovieReservation;
import com.ingeneo.cinema.co.models.ScheduleMovie;
import com.ingeneo.cinema.co.request.MovieReservationRequest;
import com.ingeneo.cinema.co.services.CinemaLocationJPA;
import com.ingeneo.cinema.co.services.MovieReservationJPA;
import com.ingeneo.cinema.co.services.ScheduleMovieJPA;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/moviereservation")
@CrossOrigin(origins="*")
@Api(value = "RestMovieReservation microservice", description = "Este API contiene un CRUD para reservación de películas")
public class RestMovieReservation {
	
	@Autowired
	MovieReservationJPA serviceMovieReservation;
	
	@Autowired
	CinemaLocationJPA serviceCinemaLocation;
	
	@Autowired
	ScheduleMovieJPA serviceScheduleMovie;

	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite guardar la reservación de una película", notes = "El nombre es requerido, excepto el id" )
	public ResponseEntity<?> save(@RequestBody @Valid MovieReservationRequest objMovieReservationRequest) {
		
		MovieReservation movieReservation = new MovieReservation();
		
		CinemaLocation cinemalocation = new CinemaLocation();	
		try {
			cinemalocation = serviceCinemaLocation.getCinemaLocationById(objMovieReservationRequest.getCinemaLocation_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		movieReservation.setCinemalocation(cinemalocation);
		
		ScheduleMovie scheduleMovie = new ScheduleMovie();	
		try {
			scheduleMovie = serviceScheduleMovie.getScheduleById(objMovieReservationRequest.getScheduleMovie_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		movieReservation.setScheduleMovie(scheduleMovie);

		return new ResponseEntity<>(serviceMovieReservation.guardar(movieReservation), HttpStatus.CREATED);

	}

	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	
	
	

}
