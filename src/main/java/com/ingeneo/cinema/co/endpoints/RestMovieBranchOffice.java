package com.ingeneo.cinema.co.endpoints;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.ingeneo.cinema.co.global.Constantes;
import com.ingeneo.cinema.co.models.BranchOffice;
import com.ingeneo.cinema.co.models.Movie;
import com.ingeneo.cinema.co.models.Movie_BranchOffice;
import com.ingeneo.cinema.co.reponse.ResponseGeneric;
import com.ingeneo.cinema.co.request.Movie_BranchOfficeRequest;
import com.ingeneo.cinema.co.services.BranchOfficeJPA;
import com.ingeneo.cinema.co.services.MovieJPA;
import com.ingeneo.cinema.co.services.Movie_BranchOfficeJPA;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/moviebranch")
@CrossOrigin(origins="*")
@Api(value = "RestMovieBranchOffice microservice", description = "Este API contiene un CRUD para relación de películas y surcursales")
public class RestMovieBranchOffice {
	
	@Autowired
	Movie_BranchOfficeJPA serviceMovieBranch;
	
	@Autowired
	MovieJPA serviceMovie;
	
	@Autowired
	BranchOfficeJPA serviceBranchOffice;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Despliega una lista de películas por sucursales", notes = "Retorna una lista global" )
	public ResponseEntity<?> listGlobal() {
		List<Movie_BranchOffice> lista = null;
		lista = serviceMovieBranch.desplegarTodos();
		if (!lista.isEmpty()) {
			return new ResponseEntity<List<Movie_BranchOffice>>(lista, HttpStatus.OK);
			
		}else {
			ResponseGeneric response = new ResponseGeneric();
			response.setStatus(false);
			response.setMessage(Constantes.NO_EXISTE);
			return new ResponseEntity<ResponseGeneric>(response, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite guardar las sucursales de perlículas", notes = "El nombre es requerido, excepto el id" )
	public ResponseEntity<?> save(@RequestBody @Valid Movie_BranchOfficeRequest objMovie_BranchOfficeRequest) {
		
		Movie_BranchOffice movie_BranchOffice = new Movie_BranchOffice();
		
		Movie movie = new Movie();	
		try {
			movie = serviceMovie.getMovieById(objMovie_BranchOfficeRequest.getMovie_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.PELICULA_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		movie_BranchOffice.setMovie(movie);
		
		BranchOffice branchOffice = new BranchOffice();	
		try {
			branchOffice = serviceBranchOffice.getBranchById(objMovie_BranchOfficeRequest.getBranchOffice_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.SUCURSAL_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		movie_BranchOffice.setBranchoffice(branchOffice);
		
		
		
		return new ResponseEntity<>(serviceMovieBranch.guardar(movie_BranchOffice), HttpStatus.CREATED);
		

	}

	
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite eliminar una sucursal asociada a una película", notes = "Debe especificar un id" )
	public ResponseEntity<?> drop(@PathVariable(name="id") Integer id) {
		
		ResponseGeneric response = new ResponseGeneric();
		HttpStatus statusResponse;
		try {
			serviceMovieBranch.eliminar(id);
			response.setStatus(true);
			response.setMessage(Constantes.ELIMINADO);
			statusResponse = HttpStatus.OK;
		}catch (Exception e) {
			response.setStatus(false);
			response.setMessage(e.getMessage());
			statusResponse = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, statusResponse);
	
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	
	
	

}
