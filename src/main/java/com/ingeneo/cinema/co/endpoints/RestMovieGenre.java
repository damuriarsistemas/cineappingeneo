package com.ingeneo.cinema.co.endpoints;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.ingeneo.cinema.co.global.Constantes;
import com.ingeneo.cinema.co.models.Genre;
import com.ingeneo.cinema.co.models.Movie;
import com.ingeneo.cinema.co.models.Movie_Genre;
import com.ingeneo.cinema.co.reponse.ResponseGeneric;
import com.ingeneo.cinema.co.request.Movie_GenreRequest;
import com.ingeneo.cinema.co.services.GenreJPA;
import com.ingeneo.cinema.co.services.MovieJPA;
import com.ingeneo.cinema.co.services.Movie_GenereJPA;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/moviegenre")
@CrossOrigin(origins="*")
@Api(value = "RestMovieGenre microservice", description = "Este API contiene un CRUD para relación de películas y géneros")
public class RestMovieGenre {
	
	@Autowired
	Movie_GenereJPA serviceMovie_Genre;
	
	@Autowired
	GenreJPA serviceGenre;
	
	@Autowired
	MovieJPA serviceMovie;

	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite guardar un genéro a una película", notes = "El nombre es requerido, excepto el id" )
	public ResponseEntity<?> save(@RequestBody @Valid Movie_GenreRequest objMovieGenreRequest) {
		
		Movie_Genre movie_Genre = new Movie_Genre ();
		Movie movie = new Movie();	
		try {
			movie = serviceMovie.getMovieById(objMovieGenreRequest.getMovie_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.PELICULA_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		movie_Genre.setMovie(movie);
		
		Genre genre = new Genre();	
		try {
			genre = serviceGenre.getGenreById(objMovieGenreRequest.getGenre_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.GENERO_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		movie_Genre.setGenre(genre);
		
		
		
		return new ResponseEntity<>(serviceMovie_Genre.guardar(movie_Genre), HttpStatus.CREATED);
		

	}

	
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite eliminar una película", notes = "Debe especificar un id" )
	public ResponseEntity<?> drop(@PathVariable(name="id") Integer id) {
		
		ResponseGeneric response = new ResponseGeneric();
		HttpStatus statusResponse;
		try {
			serviceMovie_Genre.eliminar(id);
			response.setStatus(true);
			response.setMessage(Constantes.ELIMINADO);
			statusResponse = HttpStatus.OK;
		}catch (Exception e) {
			response.setStatus(false);
			response.setMessage(e.getMessage());
			statusResponse = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, statusResponse);
	
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	
	
	

}
