package com.ingeneo.cinema.co.endpoints;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ingeneo.cinema.co.global.Constantes;
import com.ingeneo.cinema.co.models.BranchOffice;
import com.ingeneo.cinema.co.models.Cinema;
import com.ingeneo.cinema.co.models.Movie;
import com.ingeneo.cinema.co.models.ScheduleMovie;
import com.ingeneo.cinema.co.models.TypeCinema;
import com.ingeneo.cinema.co.reponse.ResponseGeneric;
import com.ingeneo.cinema.co.request.CinemaRequest;
import com.ingeneo.cinema.co.request.ScheduleMovieRequest;
import com.ingeneo.cinema.co.services.BranchOfficeJPA;
import com.ingeneo.cinema.co.services.CinemaJPA;
import com.ingeneo.cinema.co.services.GenreJPA;
import com.ingeneo.cinema.co.services.MovieJPA;
import com.ingeneo.cinema.co.services.ScheduleMovieJPA;
import com.ingeneo.cinema.co.services.TypeCinemaJPA;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/schedulemovie")
@CrossOrigin(origins="*")
@Api(value = "RestScheduleMovie microservice", description = "Este API contiene un CRUD para agendar películas")
public class RestScheduleMovie {
	
	@Autowired
	MovieJPA serviceMovie;
	
	@Autowired
	ScheduleMovieJPA serviceScheduleMovie;
	
	@Autowired
	GenreJPA serviceGenre;
	
	@Autowired
	TypeCinemaJPA serviceTypeCinema;
	
	@Autowired
	CinemaJPA serviceCinema;
	
	@Autowired
	BranchOfficeJPA serviceBranchOffice;

	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Despliega una lista de la programación", notes = "Retorna una lista global" )
	public ResponseEntity<?> listGlobal() {
		List<ScheduleMovie> lista = null;
		lista = serviceScheduleMovie.desplegarTodos();
		if (!lista.isEmpty()) {
			return new ResponseEntity<List<ScheduleMovie>>(lista, HttpStatus.OK);
			
		}else {
			ResponseGeneric response = new ResponseGeneric();
			response.setStatus(false);
			response.setMessage(Constantes.NO_EXISTE);
			return new ResponseEntity<ResponseGeneric>(response, HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite guardar una sala de cine", notes = "El nombre es requerido, excepto el id" )
	public ResponseEntity<?> save(@RequestBody @Valid ScheduleMovieRequest objScheduleMovieRequest) {
		
		
		ScheduleMovie scheduleMovie = new ScheduleMovie();
		BranchOffice branchOffice;
		try {
			branchOffice = serviceBranchOffice.getBranchById(objScheduleMovieRequest.getBranchOffice_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.SUCURSAL_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		scheduleMovie.setBranchOffice(branchOffice);
		
		Cinema cinema;
		try {
			cinema = serviceCinema.getCinemaById(objScheduleMovieRequest.getCinema_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.CINEMA_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		scheduleMovie.setCinema(cinema);
		
		Movie movie;
		try {
			movie = serviceMovie.getMovieById(objScheduleMovieRequest.getMovie_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.CINEMA_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		scheduleMovie.setMovie(movie);
		
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		try {
			scheduleMovie.setFecha(formato.parse(objScheduleMovieRequest.getFecha()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		scheduleMovie.setHora(objScheduleMovieRequest.getHora());
		
		return new ResponseEntity<>(serviceScheduleMovie.guardar(scheduleMovie), HttpStatus.CREATED);
		

	}

	
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite actualizar una sala de cine", notes = "Todos los parámetros son requeridos, debe indicar el id que desea ser actualizado" )
	public ResponseEntity<?> update(@RequestBody @Valid CinemaRequest objCinemaRequest) {
		
		Cinema cinema = new Cinema();
		try {
			cinema = serviceCinema.getCinemaById(objCinemaRequest.getId());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.CINEMA_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		
		
		BranchOffice branchOffice;
		try {
			branchOffice = serviceBranchOffice.getBranchById(objCinemaRequest.getBranchOffice_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.TIPO_SALA_CINE_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		cinema.setBranchOffice(branchOffice);
		
		cinema.setDescripCinema(objCinemaRequest.getDescripCinema());
		cinema.setNumberChairXRow(objCinemaRequest.getNumberChairXRow());
		cinema.setNumberRowXcinema(objCinemaRequest.getNumberRowXcinema());
		
		TypeCinema typeCinema;
		try {
			typeCinema = serviceTypeCinema.getTypeCinemaById(objCinemaRequest.getTypeCinema_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.TIPO_SALA_CINE_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		cinema.setTypeCinema(typeCinema);
		
		return new ResponseEntity<>(serviceCinema.guardar(cinema), HttpStatus.OK);
		
		
	}

	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite eliminar una sala de cinea", notes = "Debe especificar un id" )
	public ResponseEntity<?> drop(@PathVariable(name="id") Integer id) {
		
		ResponseGeneric response = new ResponseGeneric();
		HttpStatus statusResponse;
		try {
			serviceCinema.eliminar(id);
			response.setStatus(true);
			response.setMessage(Constantes.ELIMINADO);
			statusResponse = HttpStatus.OK;
		}catch (Exception e) {
			response.setStatus(false);
			response.setMessage(e.getMessage());
			statusResponse = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, statusResponse);
	
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	
	
	

}
