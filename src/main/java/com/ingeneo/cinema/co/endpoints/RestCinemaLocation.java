package com.ingeneo.cinema.co.endpoints;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ingeneo.cinema.co.global.Constantes;
import com.ingeneo.cinema.co.models.CinemaLocation;
import com.ingeneo.cinema.co.reponse.CinemaLocationResponse;
import com.ingeneo.cinema.co.reponse.ResponseGeneric;
import com.ingeneo.cinema.co.request.CinemaLocationRequest;
import com.ingeneo.cinema.co.services.CinemaJPA;
import com.ingeneo.cinema.co.services.CinemaLocationJPA;
import com.ingeneo.cinema.co.utils.UtilIngeneo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/cinemalocation")
@CrossOrigin(origins="*")
@Api(value = "RestCinemaLocation microservice", description = "Este API contiene un CRUD para puestos de la sala de cine")
public class RestCinemaLocation {
	
	@Autowired
	CinemaLocationJPA serviceCinemaLocation;
	
	@Autowired
	CinemaJPA serviceCinema;

	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Despliega una lista de puestos disponible", notes = "Retorna una lista global" )
	public ResponseEntity<?> listGlobal() {
		List<CinemaLocation> lista = null;
		lista = serviceCinemaLocation.desplegarTodos();
		if (!lista.isEmpty()) {
			return new ResponseEntity<List<CinemaLocation>>(lista, HttpStatus.OK);
			
		}else {
			ResponseGeneric response = new ResponseGeneric();
			response.setStatus(false);
			response.setMessage(Constantes.NO_EXISTE);
			return new ResponseEntity<ResponseGeneric>(response, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/showlocations/{filas}/{puestos}",produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Muestra los puestos disponibles de acuerdo a la configuración de una sala", notes = "Retorna una lista global" )
	public ResponseEntity<?> listLocations(@PathVariable("filas") @Min(3) @Max(27) Integer filas,@PathVariable("puestos") Integer puestos) {
		
		List<CinemaLocationResponse> listaResponse = UtilIngeneo.generarPuestos(filas,puestos);
		return new ResponseEntity<>(listaResponse, HttpStatus.OK);
	
	}
	

	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite actualizar el estado de un puesto en la sala de cine", notes = "Todos los parámetros son requeridos, debe indicar el id que desea ser actualizado" )
	public ResponseEntity<?> update(@RequestBody @Valid CinemaLocationRequest objCinemaLocationRequest) {
		
		CinemaLocation cinemaLocation = new CinemaLocation();
		try {
			cinemaLocation = serviceCinemaLocation.getCinemaLocationById(objCinemaLocationRequest.getId());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.CINEMA_PUESTO_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		
		cinemaLocation.setStatusLocation(objCinemaLocationRequest.getStatusLocation());
		return new ResponseEntity<>(serviceCinemaLocation.actualizar(cinemaLocation), HttpStatus.OK);
		
		
	}

	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite eliminar un puesto de la sala de cine", notes = "Debe especificar un id" )
	public ResponseEntity<?> drop(@PathVariable(name="id") Integer id) {
		
		ResponseGeneric response = new ResponseGeneric();
		HttpStatus statusResponse;
		try {
			serviceCinemaLocation.eliminar(id);
			response.setStatus(true);
			response.setMessage(Constantes.ELIMINADO);
			statusResponse = HttpStatus.OK;
		}catch (Exception e) {
			response.setStatus(false);
			response.setMessage(e.getMessage());
			statusResponse = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, statusResponse);
	
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	


}
