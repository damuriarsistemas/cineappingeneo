package com.ingeneo.cinema.co.endpoints;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ingeneo.cinema.co.global.Constantes;
import com.ingeneo.cinema.co.models.User;
import com.ingeneo.cinema.co.reponse.ResponseGeneric;
import com.ingeneo.cinema.co.request.UserRequest;
import com.ingeneo.cinema.co.services.UserJPA;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/user")
@CrossOrigin(origins="*")
@Api(value = "RestUser microservice", description = "Este API contiene un CRUD para registro de usuarios")
public class RestUser {
	
	@Autowired
	UserJPA serviceUser;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Despliega una lista de usuarios", notes = "Retorna una lista global" )
	public ResponseEntity<?> listGlobal() {
		List<User> lista = null;
		lista = serviceUser.desplegarTodos();
		if (!lista.isEmpty()) {
			return new ResponseEntity<List<User>>(lista, HttpStatus.OK);
			
		}else {
			ResponseGeneric response = new ResponseGeneric();
			response.setStatus(false);
			response.setMessage(Constantes.NO_EXISTE);
			return new ResponseEntity<ResponseGeneric>(response, HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite guardar una ciudad", notes = "El nombre es requerido, excepto el id" )
	public ResponseEntity<?> save(@RequestBody @Valid UserRequest objUserRequest) {
		
		
		User user = new User ();
		user.setAcount(objUserRequest.getAcount());
		user.setPassword(objUserRequest.getPassword());
		user.setRolcode(objUserRequest.getRolcode());
		return new ResponseEntity<>(serviceUser.guardar(user), HttpStatus.CREATED);
		

	}

	
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite actualizar un usuario", notes = "Todos los parámetros son requeridos, debe indicar el id que desea ser actualizado" )
	public ResponseEntity<?> update(@RequestBody @Valid UserRequest objUserRequest) {
		
		User user = new User ();
		try {
			user = serviceUser.getUserById(objUserRequest.getId());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.CIUDAD_NO_EXISTE, HttpStatus.NOT_FOUND);
		}

		user.setAcount(objUserRequest.getAcount());
		user.setPassword(objUserRequest.getPassword());
		user.setRolcode(objUserRequest.getRolcode());
		return new ResponseEntity<>(serviceUser.actualizar(user), HttpStatus.OK);
		
	}

	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite eliminar un usuario", notes = "Debe especificar un id" )
	public ResponseEntity<?> drop(@PathVariable(name="id") Integer id) {
		
		ResponseGeneric response = new ResponseGeneric();
		HttpStatus statusResponse;
		try {
			serviceUser.eliminar(id);
			response.setStatus(true);
			response.setMessage(Constantes.ELIMINADO);
			statusResponse = HttpStatus.OK;
		}catch (Exception e) {
			response.setStatus(false);
			response.setMessage(e.getMessage());
			statusResponse = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, statusResponse);
	
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	
	
	

}
