package com.ingeneo.cinema.co.endpoints;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ingeneo.cinema.co.global.Constantes;
import com.ingeneo.cinema.co.models.Movie;
import com.ingeneo.cinema.co.models.TypeCinema;
import com.ingeneo.cinema.co.reponse.ResponseGeneric;
import com.ingeneo.cinema.co.request.MovieRequest;
import com.ingeneo.cinema.co.services.GenreJPA;
import com.ingeneo.cinema.co.services.MovieJPA;
import com.ingeneo.cinema.co.services.TypeCinemaJPA;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/movie")
@CrossOrigin(origins="*")
@Api(value = "RestMovie microservice", description = "Este API contiene un CRUD para películas")
public class RestMovie {
	
	@Autowired
	MovieJPA serviceMovie;
	
	@Autowired
	GenreJPA serviceGenre;
	
	@Autowired
	TypeCinemaJPA serviceTypeCinema;

	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Despliega una lista de películas", notes = "Retorna una lista global" )
	public ResponseEntity<?> listGlobal() {
		List<Movie> lista = null;
		lista = serviceMovie.desplegarTodos();
		if (!lista.isEmpty()) {
			return new ResponseEntity<List<Movie>>(lista, HttpStatus.OK);
			
		}else {
			ResponseGeneric response = new ResponseGeneric();
			response.setStatus(false);
			response.setMessage(Constantes.NO_EXISTE);
			return new ResponseEntity<ResponseGeneric>(response, HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite guardar una película", notes = "El nombre es requerido, excepto el id" )
	public ResponseEntity<?> save(@RequestBody @Valid MovieRequest objMovieRequest) {
		
		
		Movie movie = new Movie ();
		movie.setOriginalName(objMovieRequest.getOriginalName());
		movie.setTranslationName(objMovieRequest.getTranslationName());
		movie.setReleaseDate(objMovieRequest.getReleaseDate());
		movie.setDischargeDate(objMovieRequest.getDischargeDate());
		
		TypeCinema typeCinema;
		try {
			typeCinema = serviceTypeCinema.getTypeCinemaById(objMovieRequest.getTypeCinema_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.TIPO_SALA_CINE_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		movie.setTypeCinema(typeCinema);
		
		
		movie.setDuration(objMovieRequest.getDuration());
		movie.setUrlimg(objMovieRequest.getUrlimg());
		
		return new ResponseEntity<>(serviceMovie.guardar(movie), HttpStatus.CREATED);
		

	}

	
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite actualizar una película", notes = "Todos los parámetros son requeridos, debe indicar el id que desea ser actualizado" )
	public ResponseEntity<?> update(@RequestBody @Valid MovieRequest objMovieRequest) {
		
		Movie movie = new Movie();
		try {
			movie = serviceMovie.getMovieById(objMovieRequest.getId());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.PELICULA_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		
		
		movie.setOriginalName(objMovieRequest.getOriginalName());
		movie.setTranslationName(objMovieRequest.getTranslationName());
		movie.setReleaseDate(objMovieRequest.getReleaseDate());
		movie.setDischargeDate(objMovieRequest.getDischargeDate());
		
		TypeCinema typeCinema;
		try {
			typeCinema = serviceTypeCinema.getTypeCinemaById(objMovieRequest.getTypeCinema_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.TIPO_SALA_CINE_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		movie.setTypeCinema(typeCinema);
		
		movie.setDuration(objMovieRequest.getDuration());
		movie.setUrlimg(objMovieRequest.getUrlimg());
		
		return new ResponseEntity<>(serviceMovie.guardar(movie), HttpStatus.OK);
		
	}

	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite eliminar una película", notes = "Debe especificar un id" )
	public ResponseEntity<?> drop(@PathVariable(name="id") Integer id) {
		
		ResponseGeneric response = new ResponseGeneric();
		HttpStatus statusResponse;
		try {
			serviceMovie.eliminar(id);
			response.setStatus(true);
			response.setMessage(Constantes.ELIMINADO);
			statusResponse = HttpStatus.OK;
		}catch (Exception e) {
			response.setStatus(false);
			response.setMessage(e.getMessage());
			statusResponse = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, statusResponse);
	
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	
	
	

}
