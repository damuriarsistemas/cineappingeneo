package com.ingeneo.cinema.co.endpoints;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ingeneo.cinema.co.global.Constantes;
import com.ingeneo.cinema.co.models.BranchOffice;
import com.ingeneo.cinema.co.models.City;
import com.ingeneo.cinema.co.models.User;
import com.ingeneo.cinema.co.reponse.ResponseGeneric;
import com.ingeneo.cinema.co.request.BranchOfficeRequest;
import com.ingeneo.cinema.co.services.BranchOfficeJPA;
import com.ingeneo.cinema.co.services.CityJPA;
import com.ingeneo.cinema.co.services.UserJPA;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/branchoffice")
@CrossOrigin(origins="*")
@Api(value = "BranchOffice microservice", description = "Este API contiene un CRUD para BranchOffice")
public class RestBranchOffice {
	
	@Autowired
	BranchOfficeJPA serviceBranchOffice;
	
	@Autowired
	CityJPA serviceCity;
	
	@Autowired
	UserJPA serviceUser;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Despliega una lista de sucursales", notes = "Return una lista global" )
	public ResponseEntity<?> listGlobal() {
		List<BranchOffice> lista = null;
		lista = serviceBranchOffice.desplegarTodos();
		if (!lista.isEmpty()) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
			
		}else {
			ResponseGeneric response = new ResponseGeneric();
			response.setStatus(false);
			response.setMessage(Constantes.NO_EXISTE);
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite guardar una sucursal", notes = "Todos los parámetros son requeridos, excepto el id" )
	public ResponseEntity<?> save(@RequestBody @Valid BranchOfficeRequest objBranchRequest) {
		
		
		BranchOffice branchoffice = new BranchOffice();
		branchoffice.setAddress(objBranchRequest.getAddress());
		City city; 
		try {
			city = serviceCity.getCityById(objBranchRequest.getCity_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.CIUDAD_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		branchoffice.setCity(city);
		
		branchoffice.setNameBranchOffice(objBranchRequest.getNameBranchOffice());
		User user;
		try {
			user = serviceUser.getUserById(objBranchRequest.getUser_manager_id());
		}catch (Exception e) {
			return new ResponseEntity<>(Constantes.USUARIO_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		branchoffice.setUserManager(user);
		
		return new ResponseEntity<>(serviceBranchOffice.guardar(branchoffice), HttpStatus.CREATED);
		

	}

	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite actualizar una sucursal", notes = "Todos los parámetros son requeridos, debe indicar el id que desea ser actualizado" )
	public ResponseEntity<?> update(@RequestBody @Valid BranchOfficeRequest objBranchRequest) {
		
		
		BranchOffice branchoffice = new BranchOffice();
		try {
			branchoffice = serviceBranchOffice.getBranchById(objBranchRequest.getId());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.SUCURSAL_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		
		branchoffice.setAddress(objBranchRequest.getAddress());
		City city; 
		try {
			city = serviceCity.getCityById(objBranchRequest.getCity_id());			
		} catch (Exception e) {
			return new ResponseEntity<>(Constantes.CIUDAD_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		branchoffice.setCity(city);
		
		branchoffice.setNameBranchOffice(objBranchRequest.getNameBranchOffice());
		User user;
		try {
			user = serviceUser.getUserById(objBranchRequest.getUser_manager_id());
		}catch (Exception e) {
			return new ResponseEntity<>(Constantes.USUARIO_NO_EXISTE, HttpStatus.NOT_FOUND);
		}
		branchoffice.setUserManager(user);
		
		return new ResponseEntity<>(serviceBranchOffice.actualizar(branchoffice), HttpStatus.CREATED);
		

	}

	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Permite eliminar una sucursal", notes = "Debe especificar un id" )
	public ResponseEntity<?> drop(@PathVariable(name="id") Integer id) {
		
		ResponseGeneric response = new ResponseGeneric();
		HttpStatus statusResponse;
		try {
			serviceBranchOffice.eliminar(id);
			response.setStatus(true);
			response.setMessage(Constantes.ELIMINADO);
			statusResponse = HttpStatus.OK;
		}catch (Exception e) {
			response.setStatus(false);
			response.setMessage(e.getMessage());
			statusResponse = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, statusResponse);
	
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	

}
